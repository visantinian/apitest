Test case was solved with usage of Docker-compose, API Platform and Symfony 5.
To start application you need to clone the repository to your local machine, run
 composer install
Ensure that you have complete Docker + docker-compose installed on your machine. Then run 
 make up 
 (Small configuration file that I am usually using from project to project with preconfigured small database container with predefined values for .env file)
 then as usually run 
 symfony console doctrine:schema:create
 And also, I prepared some fixtures if you would want to create some values to play with automatically by running 
 symfony console doctrine:fixtures:load   
 I configured API Platform to always use '/' route, which will be the "front" page of the application.
 After all that you can just run
 symfony server:start and you will have working simple API that would allow you to create products and update existing orders.
