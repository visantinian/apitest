<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Product;
use App\Entity\OrderItem;
use App\Entity\Order;

class OrderFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $product = new Product();
            $product->setName('Mozarella '.$i);
            $product->setPrice(mt_rand(1, 100));
            $manager->persist($product);

            $item = new OrderItem();
            $item->setProduct($product);
            $item->setQuantity(rand(1,10));
            $manager->persist($item);

            $order = new Order;
            $order->addOrderItem($item);
            $order->setTotalPrice();
            $manager->persist($order);

            $manager->flush();
        }
    }
}
