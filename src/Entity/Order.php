<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(collectionOperations={
 *     "get",
 *     "post"={
 *     "path"="/order"
 *            }
 *     },
 *     itemOperations={
 *     "get",
 *     "patch"={
 *     "path"="/order/product/add"
 *            }
 *                                   }
 *     )
 */
class Order
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=OrderItem::class, mappedBy="relatedOrder", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $orderItems;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalPrice;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class)
     */
    private $products;

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|OrderItem[]
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }


    public function addOrderItem(OrderItem $item): self
    {
        foreach ($this->getOrderItems() as $existingItem) {
            // The product already exists, update the quantity
            if ($existingItem->equals($item)) {
                $existingItem->setQuantity(
                    $existingItem->getQuantity() + $item->getQuantity()
                );
                return $this;
            }
        }
        $this->orderItems[] = $item;
        $item->setRelatedOrder($this);

        return $this;
    }


    /**
     * Calculates the order total.
     *
     * @return int
     */
    public function getTotalPrice(): ?int
    {
        $total = 0;

        foreach ($this->getOrderItems() as $item) {
            $total += $item->getTotal();
        }

        return $total;
    }
    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     *
     */
    public function setTotalPrice() {
        $this->totalPrice = $this->getTotalPrice();
        return $this;
    }


    public function removeProduct(Product $product): self
    {
        $this->products->removeElement($product);

        return $this;
    }

}
